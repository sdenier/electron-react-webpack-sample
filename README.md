# electron-react-webpack-sample

Forked from https://github.com/deadcoder0904/electron-webpack-sample

Objective is to test a simple yet customizable structure/configuration for a legacy electron project, including the following:

- **electron** platform
- **webpack** 4 to bundle main and renderer processes
- **TypeScript** and **React** as main development technologies
- **electron-builder** for packaging the application
