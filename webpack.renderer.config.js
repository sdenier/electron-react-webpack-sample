const path = require("path");
const merge = require("webpack-merge");
const HtmlWebpackPlugin = require("html-webpack-plugin");

const base = require("./webpack.base.config");
const buildPath = path.resolve(__dirname, "./build");

const renderer = merge(base, {
  entry: "./src/renderer/renderer.tsx",
  output: {
    filename: "renderer.js",
    path: buildPath
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: ["style-loader", "css-loader"]
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: "./src/renderer/index.html"
    })
  ],
  target: "electron-renderer"
});

module.exports = renderer;
